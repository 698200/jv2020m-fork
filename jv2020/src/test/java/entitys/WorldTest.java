package entitys;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class WorldTest {
	
	private static World worldTest;
	
	@BeforeAll
	public static void initializeGlobalData() {
		worldTest = new World();
	}
	
	@Test
	@DisplayName("Empiezan los tests")
	@Order(1)
	void setNameTest() {
		worldTest.setName("MundoTest");
		assertTrue(worldTest.getName() == "MundoTest");
	}
	void setDistributionTest() {
		List<Coordinate> list = new ArrayList<Coordinate>();
		list.add(new Coordinate(4,2));
		list.add(new Coordinate(1,6));
		list.add(new Coordinate(3,8));
		list.add(new Coordinate(1,1));
		worldTest.setDistribution(lista);
		assertTrue(worldTest.getDistribution() == list);
		
	}
} 
